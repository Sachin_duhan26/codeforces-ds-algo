#include <iostream>
using namespace std;

void removeX(char a[]){
    // removing the inplace x in the code
    if(a[0] == '\0')return;

    if(a[0] != 'x')removeX(a+1);
    else if(a[0] == 'x'){
        int i=1;
        while(a[i] != '\0'){
            a[i-1]=a[i];
            i++;
        }
        a[i-1] = '\0';
        removeX(a);
    }
}

int length(char a[]){
    if(a[0] == '\0'){
        return 0;
    }
    int small = length(a+1);
    return 1+small;
}

int main()
{
    char s[1000];
    cin >> s;
    cout << length(s) << endl;
    removeX(s);
    cout << s << endl;
    cout << length(s);
    return 0;
}
