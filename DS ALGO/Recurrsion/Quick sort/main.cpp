#include<iostream>
using namespace std;

int partition(int num[],int l,int r){
    if(l>=r)return -1;
    int count = 0;

    for (int i = l; i < r; i++)
    {
        if(num[i] <= num[0])count++;
    }
    
    int i,j=l;
    j=r;

    swap(num[i],num[i+count]); 
    
    while (i<=j)
    {
        if(num[i] < num[l+count])i++;
        else if (num[j] > num[l+count])j--;
        else swap(num[i],num[j]);
    }
    return l+count;
}

void swap(int a,int b){
    int k=a;
    a=b;
    b=k;
}

void QS(int num[], int start,int end){
    if(start <= end) return;
    int m = partition(num,start,end);
    QS(num,start,m-1);
    QS(num,m+1,end);
}

int main(){
    int a[] = {1,4,2,24,5,31};
    
    QS(a,0,6);
    
    for (int i = 0; i < 6; i++)
    {
        cout << a[i] << " ";
    }
    cout << endl;
    return 0;
}