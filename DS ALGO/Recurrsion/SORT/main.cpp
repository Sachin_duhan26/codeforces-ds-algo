#include <iostream>
using namespace std;

void selectionSort(int num[], int len)
{
    for (int i = 0; i < len; i++)
    {
        int min = num[i]; // 1 4 52 24 2
        for (int j = i; j < len; j++)
        {
            if (num[j] <= min)
                min = num[j];
        }
        swap(num[i], min);
    }
}

void bubbleSort(int num[], int len)
{
    for (int i = 0; i < len; i++)
    {
        for (int j = i; j < len; j++)
        {
            if (num[i] > num[j])
                swap(num[i], num[j]);
        }
    }
}

void merge(int arr[],int l,int m,int r){
     int i, j, k; 
    int n1 = m - l + 1; 
    int n2 =  r - m; 
  
    int L[n1], R[n2]; 

    for (i = 0; i < n1; i++) 
        L[i] = arr[l + i]; 
    for (j = 0; j < n2; j++) 
        R[j] = arr[m + 1+ j]; 
  
    i = 0; 
    j = 0;  
    k = l;  
    while (i < n1 && j < n2) 
    { 
        if (L[i] <= R[j]) 
        { 
            arr[k] = L[i]; 
            i++; 
        } 
        else
        { 
            arr[k] = R[j]; 
            j++; 
        } 
        k++; 
    } 
  
    while (i < n1) 
    { 
        arr[k] = L[i]; 
        i++; 
        k++; 
    } 

    while (j < n2) 
    { 
        arr[k] = R[j]; 
        j++; 
        k++; 
    } 
    
}

void mergeSort(int num[],int start,int end){
    if(start >= end)return;
    int mid = (start+end)/2;
    mergeSort(num,start,mid);
    mergeSort(num,mid+1,end);
    merge(num,start,mid,end);
}

void swap(int i, int j)
{
    int k = i;
    i = j;
    j = k;
}

int main()
{
    int num[] = {1, 4, 52, 7, 2};
    // selectionSort(num, 5);
    // bubbleSort(num,5);
    mergeSort(num,0,5);
    for (int i = 0; i < 5; i++)
    {
        cout << num[i] << " ";
    }
    cout << endl;

    return 0;
}
