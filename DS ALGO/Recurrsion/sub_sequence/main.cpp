#include <iostream>
#include <string.h>
#include <math.h>

using namespace std;

int subs(string input, string output[])
{
    if (input.empty())
    {
        output[0] = "";
        return 1;
    }
    string small = input.substr(1);
    int val = subs(small,output);
    for (int i = 0; i < val; i++)
        output[i+val] = input[0]+output[i];
    
    return 2*val;    
}

int main()
{
    // return all subsequence! of the string!
    // total number of sub sequences are = 2^(n)

    string n;
    cin >> n;

    string *output = new string[1000];
    int size = subs(n, output);
    cout << size << endl;
    for (int i = 0; i < size; i++)
        cout << output[i] << " ";

    cout << endl;
    return 0;
}