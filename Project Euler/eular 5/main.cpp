#include <iostream>

using namespace std;

int gcd(int a, int b){
    if (a == 0) return b;
    return gcd(b % a, a);
}

int lcm(int a, int b){
    return (a*b)/gcd(a, b);
}

int main()
{
    int res = 2520;
    for(int i=11;i<21;i++){
        res = lcm(i,res);
    }
    cout << res << endl;
    return 0;
}
