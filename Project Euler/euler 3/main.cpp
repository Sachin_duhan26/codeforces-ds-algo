#include <bits/stdc++.h>
using namespace std;

bool check(int value){
    string val = to_string(value);
    for(int i=0;i<val.size()/2;i++){
        if(val[i] != val[val.size()-i-1])return false;
    }
    return true;
}

int main()
{
    int start = 100;
    int last = 999;
    /*finding the largest palindrome of three digits*/

    int fix = 999;
    int res = 0;
    int a = 0;
    int b = 0;
    while(fix > 100){
        bool flag = false;
        for(int i=999;i>100;i--){
        int val = fix*i;
            if(check(val) && res <= val){
                a = fix;
                b = i;
                res = val;
            };
        }
        fix--;
    }
    cout << a << " * "<< b << " = " << res << endl;
    return 0;
}
